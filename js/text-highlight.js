/**
 * Custom text-highlight functionality.
 *
 * Adds an intersection observer to highlight marked text only when in the visible viewport.
 */
(Drupal => {
  /**
   * Handle highlighting the observed object.
   *
   * @param {IntersectionObserverEntry} entries  - The entries being observed.
   * @param {IntersectionObserver}      observer - The observer.
   */
  function highlight(entries, observer) {
    // There is always an array of entries passed, so we need to loop through them.
    entries.forEach(entry => {
      // First we need to check if the entry is truely intesecting.
      // If it is, then we can add our class to animate the text highlight.
      if (entry.isIntersecting) {
        setTimeout(() => {
          entry.target.classList.add("active");
        }, 150);

        // Once our animation has happened, we can stop observing this element
        // to save resources on the client's end. It's best practice to do this.
        observer.unobserve(entry.target);
      }
    })
  }

  // Observer options.
  const options = {
    rootMargin: "0px",
    threshold: 0.1,
  };

  // Observer.
  const observer = new IntersectionObserver(highlight, options);

  Drupal.behaviors.feaTextHightlight = {
    attach: (context) => {

      once("feaAnimations", "div.paragraph--type--text-block mark", context).forEach(element => {
        // Set a custom animation length based on the length of text.
        const highlightDuration = 1;
        const animationLength = element.innerHTML.length * highlightDuration;
        element.style.transition = `background-size ${animationLength}ms ease-in`;

        // Observe element.
        observer.observe(element);
      });
    }
  };
})(Drupal);

